import re
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.metrics.pairwise import cosine_similarity

from src.config import STOP_TAGS
from src.initialize import morph


def get_text_by_file_name(filename: str) -> str:
    with open(f'../data/{filename}') as f:
        return f.read()


def get_normalized_text_words(text: str) -> list:
    clear_text = re.sub(r'[^\w\s]', '', text)
    words = []
    for word in clear_text.split():
        word_info = morph.parse(word)[0]
        if not any([stop_tag in word_info.tag for stop_tag in STOP_TAGS]):
            words.append(word_info.normal_form)
    return list(set(words))


def get_jaccard_index(first: list, second: list) -> float:
    first, second = set(first), set(second)
    return len(first.intersection(second)) / len(first.union(second))


def get_cosine_similarity(first: list, second: list) -> float:
    count_vectorizer = CountVectorizer()
    vector_matrix = count_vectorizer.fit_transform([' '.join(first), ' '.join(second)])
    cosine_similarity_matrix = cosine_similarity(vector_matrix)
    return cosine_similarity_matrix[0][1]


def print_pretty_result(jaccard_indexes: dict, cosine_indexes: dict) -> None:
    max_jaccard_index = max(jaccard_indexes.values())
    print('\033[1mJaccard Indexes:\033[0m')
    for topic, index in jaccard_indexes.items():
        topic = f'- {topic.capitalize()}'
        print(f'\033[0;32m{topic}: {index}\033[0m' if index == max_jaccard_index else f'{topic}: {index}')

    max_cosine_index = max(cosine_indexes.values())
    print('\n\033[1mCosine Indexes:\033[0m')
    for topic, index in cosine_indexes.items():
        topic = f'- {topic.capitalize()}'
        print(f'\033[0;32m{topic}: {index}\033[0m' if index == max_cosine_index else f'{topic}: {index}')
