from src.config import DEFAULT_TEXT_FILE_NAME, KEY_WORDS
from src.utils import (
    get_text_by_file_name, get_normalized_text_words, get_jaccard_index, get_cosine_similarity,
    print_pretty_result,
)


def main():
    text = get_text_by_file_name(DEFAULT_TEXT_FILE_NAME)
    normalized_words = get_normalized_text_words(text)

    key_words = {
        'science': get_normalized_text_words(KEY_WORDS['science']),
        'sport': get_normalized_text_words(KEY_WORDS['sport']),
        'shopping': get_normalized_text_words(KEY_WORDS['shopping']),
        'news': get_normalized_text_words(KEY_WORDS['news']),
    }

    jaccard_indexes = {}
    cosine_indexes = {}
    for topic, topic_words in key_words.items():
        jaccard_indexes[topic] = get_jaccard_index(normalized_words, topic_words)
        cosine_indexes[topic] = get_cosine_similarity(normalized_words, topic_words)

    print_pretty_result(jaccard_indexes, cosine_indexes)


if __name__ == '__main__':
    main()
